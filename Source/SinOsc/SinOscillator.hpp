//
//  SinOscillator.hpp
//  JuceBasicAudio
//
//  Created by conor sweetingham on 07/11/2017.
//
//

#ifndef SinOscillator_hpp
#define SinOscillator_hpp

#include <stdio.h>
#include "../../JuceLibraryCode/JuceHeader.h"


class SinOscillator
{
public:
    //==============================================================================
    /**
     SinOscillator constructor
     */
    SinOscillator();
    
    /**
     SinOscillator destructor
     */
    ~SinOscillator();
    
    /**
     sets the frequency of the oscillator
     */
    void setFrequency (float freq);
    
    /**
     sets frequency using a midi note number
     */
    void setNote (int noteNum);
    
    /**
     sets the amplitude of the oscillator
     */
    void setAmplitude (float amp);
    
    /**
     resets the oscillator
     */
    void reset();
    
    /**
     sets the sample rate
     */
    void setSampleRate (float sr);
    
    /**
     Returns the next sample
     */
    float nextSample();
    
    /**
     function that provides the execution of the waveshape
     */
    float renderWaveShape (const float currentPhase);
    
    float frequency;
    float amplitude;
    float sampleRate;
    float phase;
    float phaseInc;
    
private:

};

#endif //H_SINOSCILLATOR