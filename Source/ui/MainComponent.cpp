/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& a) : Thread ("CounterThreat"),audio (a)
{
    setSize (500, 400);
    /*add that button bwoii*/
    button1.setButtonText("Start/Stop");
    addAndMakeVisible(button1);
    button1.addListener(this);
    
    
    startThread();
}

MainComponent::~MainComponent()
{
    stopThread(500);
}

void MainComponent::resized()
{
    button1.setBounds(10, 10, getWidth() - 20, 40);
}
void MainComponent::buttonClicked(Button* button)
{
    DBG("Button Clicked\n");
}
void MainComponent::run()
{
    while (!threadShouldExit())
    {
        uint32 time = Time::getApproximateMillisecondCounter();
        std::cout << "Counter:" << Time::getApproximateMillisecondCounter() << "\n";
        Time::waitForMillisecondCounter(time + 100);
    }
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

